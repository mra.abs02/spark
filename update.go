package spark

import "time"

type uTime time.Time

// LastUpdate is the last time the database has been updated
var LastUpdate uTime

// Public provides the public view of the database last update time
func (l *uTime) public() interface{} {
	return map[string]interface{}{
		"last_update": l,
	}
}
