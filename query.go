package spark

/* //MY SQL TABLE FEILDS
 CREATE TABLE ques (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
question TINYTEXT NOT NULL,
description TEXT,
src VARCHAR(50),
url TINYTEXT,
rating INT(6) UNSIGNED DEFAULT 1
)*/

/* Report table structure
CREATE TABLE report (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    ques TINYTEXT,
    qid INT(60),
	reason TINYTEXT,
	who TINYTEXT,
    done BIT DEFAULT 0);

*/
/*
CREATE TABLE spark_users (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name TINYTEXT,
	email TINYTEXT,);

*/
/* To get the last question in the table use this query in mysql
SELECT id FROM ques ORDER BY id DESC LIMIT 1;
*/

// Query contains the feild for each question
type Query struct {
	ID     int
	Ques   string
	Desc   string
	Src    string
	URL    string
	Rating int
}

// IF the public view gets longer and longer create a public interface like

// Public returns the public view of each question
func (q *Query) Public() interface{} {
	return map[string]interface{}{
		"id":     q.ID,
		"ques":   q.Ques,
		"desc":   q.Desc,
		"src":    q.Src,
		"url":    q.URL,
		"rating": q.Rating,
	}
}

// JUST INCASE I NEED TO INTIALIZE THE VALUES

// func NewQuery(ques string, desc string, src string, URL string, rating int) query {
// 	if src == "" {
// 		src = "Public Domain"
// 	}
// 	if rating == 0 {
// 		rating = 1
// 	}
// 	return query{
// 		Ques:   ques,
// 		Desc:   desc,
// 		Src:    src,
// 		URL:    URL,
// 		Rating: rating,
// 	}
// }
