package spark

// Facade provde an interface to the public view of the data
type Facade interface {
	Public() interface{}
}

// Public gets the public representation of the specified object
// if it implements the Facade interface. Otherwise returns the
// object untouched.
func Public(o interface{}) interface{} {
	if p, ok := o.(Facade); ok {
		return p.Public()
	}
	return o
}
