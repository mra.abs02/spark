package main

/*
TODO add CORS
TODO add login page
TODO add question insertion

*/
import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"spark"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var lastUpdate time.Time

func init() {
	lastUpdate = time.Now()
}

// the data base is created using "spark:spark123@tcp(127.0.0.1)/Ques"
const mysqlLoggin = "spark:spark123@@tcp(127.0.0.1)/Ques"

//  Gets all the questions from the database.
func getQues(db *sql.DB) (interface{}, error) {
	rows, err := db.Query("SELECT id, question, description, src, url, rating FROM ques")
	if err != nil {
		log.Println("Error in querying the database.")
		return nil, err
	}
	defer rows.Close()
	ques := make([]interface{}, 0)

	for rows.Next() {
		q := new(spark.Query)
		err := rows.Scan(&q.ID, &q.Ques, &q.Desc, &q.Src, &q.URL, &q.Rating)
		if err != nil {
			log.Println("Error in Scaning the querying.")
			return nil, err
		}
		ques = append(ques, q.Public())
	}
	return ques, nil
}

// To insert a new question.
func insertIntoDB(db *sql.DB, q *spark.Query) error {
	query := `INSERT INTO ques (question, description, src, url, rating) VALUES (?, ?, ?, ?, ?)`

	res, err := db.Exec(query, q.Ques, q.Desc, q.Src, q.URL, q.Rating)
	if err != nil {
		return err
	}

	log.Println(res.LastInsertId())
	lastUpdate = time.Now()

	return nil
}

func response(w http.ResponseWriter, r *http.Request, data interface{}) error {
	return json.NewEncoder(w).Encode(data)
}

// To get the questions since the last update.
func sinceLastUpdate(db *sql.DB, id string) (interface{}, error) {
	sqlq := fmt.Sprintf("SELECT id, question, description, src, url, rating FROM ques where id>%s", id)
	rows, err := db.Query(sqlq)
	if err != nil {
		log.Println("Error in querying the database.")
		return nil, err
	}
	defer rows.Close()
	ques := make([]interface{}, 0)

	for rows.Next() {
		q := new(spark.Query)
		err := rows.Scan(&q.ID, &q.Ques, &q.Desc, &q.Src, &q.URL, &q.Rating)
		if err != nil {
			log.Println("Error in Scaning the querying.")
			return nil, err
		}
		ques = append(ques, q.Public())
	}
	return ques, nil
}

// To increase a questions rating.
func incRating(db *sql.DB, id string) error {
	sqlq := fmt.Sprintf("UPDATE ques SET rating = rating + 1 WHERE id = %s", id)
	res, err := db.Exec(sqlq)
	if err != nil {
		log.Printf("Error while updating the rating.")
		return err
	}
	log.Println(res.LastInsertId())

	return nil
}

// To reduce a questions rating.
func decRating(db *sql.DB, id string) error {
	sqlq := fmt.Sprintf("UPDATE ques SET rating = rating - 1 WHERE id = %s", id)
	res, err := db.Exec(sqlq)
	if err != nil {
		log.Printf("Error while updating the rating.")
		return err
	}
	log.Println(res.LastInsertId())

	return nil
}

// To report a question.
func reportQues(db *sql.DB, ques, id, reason, user string) error {
	query := `INSERT INTO report (ques, qid, reason, who) VALUES (?, ?, ?, ?)`

	res, err := db.Exec(query, ques, id, reason, user)
	if err != nil {
		return err
	}

	log.Println(res.LastInsertId())

	return nil
}

// To register a user in the database.
func regUser(db *sql.DB, name, email string) error {
	query := `INSERT INTO spark_users (name, email) VALUES (?, ?)`

	res, err := db.Exec(query, name, email)
	if err != nil {
		return err
	}

	log.Println(res.LastInsertId())

	return nil

}

// To deregister a user from the database.
func deregUser(db *sql.DB, email string) error {

	query := fmt.Sprintf("DELETE FROM spark_users WHERE email='%s'", email)

	res, err := db.Exec(query)
	if err != nil {
		return err
	}

	log.Println(res.LastInsertId())

	return nil

}

func main() {

	db, err := sql.Open("mysql", mysqlLoggin)
	if err != nil {
		log.Print("Error opening the database.")
		log.Fatalln("err")
	}
	log.Println("Connected to the database.")
	defer db.Close()

	q := &spark.Query{
		Ques: "What are you passionate about and want to spend more time doing?",
		Src:  "Public Domain",
	}

	err = db.Ping()
	if err != nil {
		log.Print("Error while pinging the database.")
		log.Fatalln(err)
	}

	// lastUpdate = time.Now()
	// log.Println(checkLastUpdate())

	// err = insertIntoDB(db, q)
	// if err != nil {
	// 	log.Print("Unable to insert data into the database. ")
	// 	log.Println(err)
	// }

	// This path gets all the database information.
	http.HandleFunc("/get", func(w http.ResponseWriter, r *http.Request) {
		ques, err := getQues(db)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		response(w, r, ques)
	})

	// This path gets the last time the database has been updated.
	http.HandleFunc("/lastupdate", func(w http.ResponseWriter, r *http.Request) {
		last := map[string]time.Time{
			"last_updated": lastUpdate,
		}
		response(w, r, last)
	})

	// This path inserts a new question into the database.
	http.HandleFunc("/insert", func(w http.ResponseWriter, r *http.Request) {
		var err error
		q.Ques = r.URL.Query().Get("ques")
		if q.Ques == "" {
			http.Error(w, "Question cannot be empty.", http.StatusBadRequest)
			return
		}
		q.Desc = r.URL.Query().Get("desc")
		q.Src = r.URL.Query().Get("src")
		q.URL = r.URL.Query().Get("url")

		err = insertIntoDB(db, q)
		if err != nil {
			log.Print("Unable to insert data into the database. ")
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Println("successfully inserted the question")
	})

	http.HandleFunc("/sincelast", func(w http.ResponseWriter, r *http.Request) {
		id := r.URL.Query().Get("id")
		if id == "" {
			http.Error(w, "Id should not be empty", http.StatusBadRequest)
			return
		}
		var err error
		ques, err := sinceLastUpdate(db, id)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		response(w, r, ques)
	})

	http.HandleFunc("/incrating", func(w http.ResponseWriter, r *http.Request) {
		var err error
		id := r.URL.Query().Get("id")
		if id == "" {
			http.Error(w, "id should not be empty", http.StatusBadRequest)
			return
		}
		err = incRating(db, id)
		if err != nil {
			log.Println(err)
			http.Error(w, "Error in incrementing the rating", http.StatusInternalServerError)
			return
		}
	})

	http.HandleFunc("/decrating", func(w http.ResponseWriter, r *http.Request) {
		var err error
		id := r.URL.Query().Get("id")
		if id == "" {
			http.Error(w, "id should not be empty", http.StatusBadRequest)
			return
		}
		err = decRating(db, id)
		if err != nil {
			log.Println(err)
			http.Error(w, "Error in decrementing the rating", http.StatusInternalServerError)
			return
		}
	})

	http.HandleFunc("/report", func(w http.ResponseWriter, r *http.Request) {
		var err error
		ques := r.URL.Query().Get("ques")
		if ques == "" {
			http.Error(w, "Question cannot be empty.", http.StatusBadRequest)
			return
		}

		id := r.URL.Query().Get("id")
		if id == "" {
			http.Error(w, "id cannot be empty.", http.StatusBadRequest)
			return
		}
		res := r.URL.Query().Get("res")
		if res == "" {
			http.Error(w, "Reason cannot be empty.", http.StatusBadRequest)
			return
		}
		user := r.URL.Query().Get("user") //who has reported it
		err = reportQues(db, ques, id, res, user)
		if err != nil {
			log.Print("Unable to fill the report into the database. ")
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Println("successfully reported the question.")
	})

	http.HandleFunc("/reguser", func(w http.ResponseWriter, r *http.Request) {
		var err error
		name := r.URL.Query().Get("name")
		if name == "" {
			http.Error(w, "Question cannot be empty.", http.StatusBadRequest)
			return
		}

		email := r.URL.Query().Get("email")
		if email == "" {
			http.Error(w, "id cannot be empty.", http.StatusBadRequest)
			return
		}

		err = regUser(db, name, email)
		if err != nil {
			log.Printf("Unable to register the user (%s, %s)\n", name, email)
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Printf("successfully registered the user (%s, %s).\n", name, email)
	})
	http.HandleFunc("/dereguser", func(w http.ResponseWriter, r *http.Request) {
		var err error

		name := r.URL.Query().Get("name")

		email := r.URL.Query().Get("email")
		if email == "" {
			http.Error(w, "id cannot be empty.", http.StatusBadRequest)
			return
		}

		err = deregUser(db, email)
		if err != nil {
			log.Printf("Unable to register the user (%s, %s)\n", name, email)
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Printf("successfully registered the user (%s, %s).\n", name, email)
	})

	log.Println("Serving Spark API on :8080")
	http.ListenAndServe(":8080", http.DefaultServeMux)

}
