# The API server for the Questions Mobile Game

## Requirements:
Go v 1.11+

MySql

## To insert a question  
  - example url:`http://localhost:8080/insert?ques=What question would you ask a fortune teller?&desc=from the big talk app&src=public domain&url=http://hello.com`
  - `ques` should not be empty otherwise it raises an error of StatusBadRequest

  - possible errors: http.StatusInternalServerError

## To fetch all the question from the database
  - url example: `http://localhost:8080/get`
  - example output : *the output is a list of map in json format*
  ```json
  [
  {"desc":"","id":1,"ques":"Do you like your name and why?","rating":0,"src":"Internet","url":""},
  {"desc":"","id":2,"ques":"What have you witnessed that has strengthened your faith in humanity?","rating":0,"src":"Public Domain","url":""},{"desc":"","id":3,"ques":"Who are your heroes?","rating":0,"src":"Public Domain","url":""},
  ]
  ```
   - possible errors: StatusInternalServerError

## To get the questions since the last update:
  - url example: `http://localhost:8080/sincelast?id=3`

  - `id` should not be empty, otherwise it raises an error of StatusBadRequest
  - Other possible errors: StatusInternalServerError

  - example output: the output is a list of map in json format
  ```json
  [{"desc":"","id":4,"ques":"What are you passionate about and want to spend more time doing?","rating":0,"src":"Public Domain","url":""},{"desc":"","id":5,"ques":"What are you passionate about and want to spend more time doing?","rating":0,"src":"Public Domain","url":""},{"desc":"from the big talk app","id":6,"ques":"What risk are you happy that you took?","rating":3,"src":"public domain","url":"http://hello.com"},{"desc":"from the big talk app","id":7,"ques":"What question would you ask a fortune teller?","rating":3,"src":"public domain","url":"http://hello.com"}]
  ``` 


## To get the last update date
  - example url: `http://localhost:8080/lastupdate`
  - example output: 
  ```json
  {"last_updated":"2019-03-03T04:49:59.024521433+05:30"}
  ```

##  To increment a question's rating
  - example url: `http://localhost:8080/incrating?id=2`
  - `id` should not be empty, otherwise it raises an error of StatusBadRequest
  - other possible errors: StatusInternalServerError (if the rating is already zero)

## To decrement a question's rating
  - example url: `http://localhost:8080/decrating?id=2`
  - `id` should not be empty, otherwise it raises an error of StatusBadRequest
  - Other possible errors: StatusInternalServerError (if the rating is already zero)


## to report a question
  - url example: `http://localhost:8080/report?ques=What question would you ask a fortune teller?&res=because this question is inappropriate&id=7,user=ex@example.com`
  - `ques`, `res`, `id` are compulsery otherwise it raises StatusBadRequest error
  - Other possible errors StatusInternalServerError

## to register a user
  - url example: `http://localhost:8080/reguser?name=alex&email=alex@example.com`
  - Both the `name` and `email` are compulsery, otherwise it raises StatusBadRequest error
  - Other possible errors StatusInternalServerError

## to deregister a user
  - url example: `http://localhost:8080/dereguser?name=alex&email=alex@example.com`
  - Only `email` is compulsery, otherwise it raises StatusBadRequest error
  - Other possible errors StatusInternalServerError




